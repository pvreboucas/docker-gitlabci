# Curso de Gitlab CI e Docker: Pipeline de entrega contínua

[Curso de Gitlab CI e Docker: Pipelines de Entrega Contínua da formação DevOps/Alura](https://cursos.alura.com.br/course/gitlab-docker-integracao-continua)

[<< CURSO ANTERIOR](https://github.com/pvreboucas/docker-jenkins)

[Principal](https://gitlab.com/pvreboucas/docker-gitlabci)

* [Aula 1 - Conhecendo gitlab CI:CD](https://gitlab.com/pvreboucas/docker-gitlabci/-/tree/aula-01)

* [Aula 2 - Versionando com gitlab](https://gitlab.com/pvreboucas/docker-gitlabci/-/tree/aula-02)

* [Aula 3 - Iniciando configuração com o gitlab-ci](https://gitlab.com/pvreboucas/docker-gitlabci/-/tree/aula-03)

* [Aula 4 - Conhecendo o gitlab-runner](https://gitlab.com/pvreboucas/docker-gitlabci/-/tree/aula-04)

* [Aula 5 - Configurando stage de testes e dependências](https://gitlab.com/pvreboucas/docker-gitlabci/-/tree/aula-05)

* [Aula 6 - Deploy com gitlab CD](https://gitlab.com/pvreboucas/docker-gitlabci/-/tree/aula-06)

* [Aula 7 - Notificações com gitlab e slack](https://gitlab.com/pvreboucas/docker-gitlabci/-/tree/aula-07)



